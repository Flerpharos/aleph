from lang.scanner import Scanner
from lang.compiler import Compiler
from lang.vmachine import VMachine
from lang.error import ScanError, CompileError, RuntimeError

scanner = Scanner()
compiler = Compiler()
vm = VMachine()

with open("test.am") as f:
    scanner.text = f.read()
try:
    scanner.execute()

    compiler.setTokens(scanner.tokens)
    compiler.execute()

    vm.setChunk(compiler.chunk)

    print(vm.execute())
except RuntimeError as err:
    err.printMessage()
    compiler.printOut()
except CompileError as err:
    err.printMessage()
    compiler.printOut()
    print(scanner.tokens)
except ScanError as err:
    err.printMessage()
except Exception as err:
    print(err)
    compiler.printOut()
    