from lang.token_type import TokenType

class Token:
    def __init__(self, token_type, value):
        assert type(token_type) is TokenType

        self.__tokentype = token_type
        self.__value = value

    @property
    def tokenType(self):
        return self.__tokentype

    @property
    def value(self):
        return self.__value

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return "TOKEN: {}| {}".format(repr(self.__tokentype), str(self.__value))