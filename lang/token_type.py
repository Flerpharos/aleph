from enum import Enum

class TokenType(Enum):
    def __repr__(self):
        return str(self)[10:]

    PLUS = 0
    DASH = 1
    CHEVRON = 2
    STAR = 3
    SLASH = 4
    SEMI = 5
    COLON = 6
    DOT = 7
    COMMA = 8
    STRING = 9
    IDENT = 10
    NUMBER = 11
    EQUALS = 12
    LPAREN = 13
    RPAREN = 14
    LBRACK = 15
    RBRACK = 16
    LCURLY = 17
    RCURLY = 18
    LESS = 19
    MORE = 20
    LESSEQ = 21
    MOREEQ = 22
    PERCENT = 23
    AS = 24
    IS = 25
    ELSE = 26
    IF = 27
    WHILE = 28
    WITH = 29
    DEFINE = 30
    RETURN = 31
    IMPORT = 32
    TRUE = 33
    FALSE = 34
    ERROR = 35
    NOT = 36
    MACRO = 37
    NULL = 38
    PRINT = 39
    END = 40
    