from enum import Enum
class Opcode(Enum):

    #Arithmetic
    ADD = 0
    SUB = 1
    MULT = 2
    DIV = 3
    POW = 4

    #Variables
    MAKE = 5
    CREATE = 6
    SET = 7
    GET = 8

    #Local Variables
    LMAKE = 9
    LCREATE = 10
    LSET = 11
    LGET = 12

    #BaseTypes
    TRUE = 13
    FALSE = 14
    NULL = 15

    #Control Flow
    IF = 16
    IFN = 17

    #Technical
    GOTO = 18
    RETURN = 19
    POP = 20
    PUSH = 21
    PRINT = 22

    #Operators
    NOT = 23
    NEGATE = 24
    EQUALS = 25
    LESS = 26
    MORE = 27
    LESSEQ = 28
    MOREEQ = 29

    #Arrays
    MKARRAY = 30
    PUSHARR = 31
    GETARR = 32
    EDITARR = 33
    




