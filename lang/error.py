class Error(Exception):

    def __init__(self, message):
        self.__message = message

    def printMessage(self):
        print(self.__class__.__name__, ":", self.__message)

    @property
    def message(self):
        return self.__message

class ScanError(Error):
    pass

class CompileError(Error):
    pass

class RuntimeError(Error):
    pass