from lang.opcode import Opcode as OP
from lang.error import RuntimeError
from lang.value import Value

class VMachine:
    
    def __init__(self, stacktrace = False):
        self.__stack = []
        self.__chunk = []
        self.__stacktrace = stacktrace
        self.__atLine = 0
        self.__ip = 0

    def execute(self):

        def getOP(opcode):
            if isinstance(opcode, int):
                return OP(opcode)
            elif isinstance(opcode, float):
                return Value(opcode)
            elif isinstance(opcode, str):
                return Value(opcode[1:-1]) if opcode[0] == '"' else opcode

        while True:                            
            instruction = getOP(self.readNext())                

            if instruction == OP.RETURN:
                return self.stack.pop().value
            elif instruction == OP.PRINT:
                print(self.__stack.pop())
            elif instruction == OP.POP:
                self.__stack.pop()
            elif instruction == OP.NEGATE:
                a = self.__stack.pop()
                self.__stack.append(a.neg())
            elif instruction == OP.ADD:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.add(b))
            elif instruction == OP.SUB:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.sub(b))
            elif instruction == OP.MULT:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.mul(b))
            elif instruction == OP.DIV:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.div(b))
            elif instruction == OP.POW:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.pow(b))
            elif instruction == OP.NOT:
                a = self.__stack.pop()
                self.__stack.append(a.negate())
            elif instruction == OP.EQUALS:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.eq(b))
            elif instruction == OP.LESS:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.lt(b))
            elif instruction == OP.MORE:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.gt(b))
            elif instruction == OP.LESSEQ:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.le(b))
            elif instruction == OP.MOREEQ:
                b = self.__stack.pop()
                a = self.__stack.pop()
                self.__stack.append(a.ge(b))
            elif instruction == OP.MKARRAY:
                self.__stack.append(Value([]))
            elif instruction == OP.PUSHARR:
                b = self.__stack.pop()
                a = self.__stack.pop().value
                if not isinstance(a, list):
                    raise RuntimeError("cannot append to non array")
                a.append(b)
                self.__stack.append(Value(a))
            elif instruction == OP.GETARR:
                b = self.__stack.pop()
                a = self.__stack.pop().value
                if not isinstance(a, list) and not isinstance(a, str):
                    raise RuntimeError("cannot index non array or string")
                try:
                    self.__stack.push(Value(a[b.value]))
                except Exception as err:
                    raise RuntimeError(str(err))
            elif instruction == OP.EDITARR:
                c = self.__stack.pop().value
                b = self.__stack.pop().value
                a = self.__stack.pop().value
                if not isinstance(a, list) and not isinstance(a, str):
                    raise RuntimeError("cannot index non array or string")
                try:
                    a[b] = c
                except Exception as err:
                    raise RuntimeError(str(err))
                self.__stack.push(Value(a))
            elif instruction == OP.TRUE:
                self.stack.append(Value(True))
            elif instruction == OP.FALSE:
                self.stack.append(Value(False))
            elif instruction == OP.NULL:
                self.stack.append(Value(None))
            elif isinstance(instruction, Value):
                self.stack.append(instruction)
            else:
                raise RuntimeError("unknown instruction.")
            
            if self.__stacktrace: 
                print("STACK: ", end="")            
                for item in self.stack:
                    print(item.value, end=",")
                print("")

    def readNext(self):
        self.__ip += 1
        return self.__chunk[self.__ip - 1]

    def setChunk(self, other):
        self.__chunk = other

    @property
    def stack(self):
        return self.__stack
