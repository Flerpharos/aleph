from lang.error import ScanError
from lang.token import Token
from lang.token_type import TokenType as T

class Scanner:
    alpha = [chr(i) for i in range(65, 91)] + [chr(i) for i in range(97, 123)] + ['_']
    numeric = [chr(i) for i in range(48, 58)]
    keywords = {
        "as": T.AS,
        "with": T.WITH,
        "is": T.IS,
        "if": T.IF,
        "while": T.WHILE,
        "with": T.WITH,
        "return": T.RETURN,
        "import": T.IMPORT,
        "false": T.FALSE,
        "true": T.TRUE,
        "define": T.DEFINE,
        "not": T.NOT,
        "macro": T.MACRO,
        "null": T.NULL,
        "print": T.PRINT
    }
    operators = {
        '+': T.PLUS,
        '-': T.DASH,
        '^': T.CHEVRON,
        '*': T.STAR,
        '/': T.SLASH,
        ';': T.SEMI,
        ':': T.COLON,
        '.': T.DOT,
        '(': T.LPAREN,
        ')': T.RPAREN,
        '[': T.LBRACK,
        ']': T.RBRACK,
        '{': T.LCURLY,
        '}': T.RCURLY,    
        '%': T.PERCENT,
        ',': T.COMMA,
        '=': T.EQUALS
    }

    def __init__(self, string=None):
        assert type(string) is str or string is None

        self.__string = string
        self.__tokens = []
        self.__charat = 0

    def execute(self):

        while self.__charat < len(self.__string):
            char = self.getChar()

            try:
                if char == " " or char == "\n":
                    self.__charat += 1
                elif char in Scanner.operators:
                    self.__tokens.append(Token(Scanner.operators[char], None))
                    self.__charat += 1
                elif char == '<':
                    if self.peek() == "=":
                        self.__tokens.append(Token(T.LESSEQ, None))
                        self.__charat += 2
                    else:
                        self.__tokens.append(Token(T.LESS, None))
                        self.__charat += 1
                elif char == '>':
                    if self.peek() == "=":
                        self.__tokens.append(Token(T.MOREEQ, None))
                        self.__charat += 2
                    else:
                        self.__tokens.append(Token(T.MORE, None))
                        self.__charat += 1
                elif char in Scanner.alpha:
                    self.identifier()
                elif char == '"':
                    self.string()
                elif char in Scanner.numeric:
                    self.number()
                else:
                    raise ScanError("Unidentified literal character at " + str(self.__charat) + "(" + char + ")")
            except ScanError as err:
                self.__tokens.append(Token(T.ERROR, err.message)) 
        
        self.__tokens.append(Token(T.END, None))

    def identifier(self):
        value = ''

        while True:
            if self.atEnd():
                break

            if self.getChar() not in (Scanner.alpha + Scanner.numeric):
                break
            char = self.getChar()

            value += char
            self.__charat += 1

        if value not in Scanner.keywords:
            self.__tokens.append(Token(T.IDENT, value))
        else:
            self.__tokens.append(Token(Scanner.keywords[value], None))

    def string(self):
        value = ''

        self.__charat += 1

        while True:

            if self.atEnd(): raise ScanError("unterminated string at" + str(self.__charat))

            char = self.getChar()

            if char == "\n": raise ScanError("unterminated string at" + str(self.__charat))

            self.__charat += 1
           
            if char != '"':
                value += char
            else:
                break

        self.__tokens.append(Token(T.STRING, value))

    def number(self):
        value = ''

        decimal = False
        #fix decimal place system
        while not self.atEnd() and (self.getChar() in Scanner.numeric or (self.getChar() == '.' and decimal == False)):
            char = self.getChar()

            value += char
            if char == '.': decimal = True

            self.__charat += 1

        self.__tokens.append(Token(T.NUMBER, float(value)))

    def getChar(self):
        return self.__string[self.__charat]

    def peek(self):
        return self.__string[self.__charat + 1] if len(self.__string) >= self.__charat + 1 else None

    def atEnd(self):
        return len(self.__string) == self.__charat

    @property
    def tokens(self):
        return self.__tokens

    @property
    def text(self):
        return self.__string

    @text.setter
    def text(self, a):
        assert type(a) is str

        self.__string = a
        self.__tokens = []