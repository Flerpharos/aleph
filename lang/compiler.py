from lang.token import Token
from lang.token_type import TokenType as T
from lang.opcode import Opcode as OP
from lang.prec import Prec
from lang.parserule import ParseRule
from lang.error import ScanError, CompileError

class Compiler:

    def __init__(self):
        self.__tokens = []
        self.__codes = []
        self.__tokenAt = 0
        self.__current = None
        self.__previous = None
        self.__parseRules = [
            ParseRule(None, self.binary, Prec.TERM),       #T.PLUS
            ParseRule(self.unary, self.binary, Prec.TERM), #T.DASH
            ParseRule(None, self.binary, Prec.EXPONENT),   #T.CHEVRON
            ParseRule(None, self.binary, Prec.FACTOR),     #T.STAR
            ParseRule(None, self.binary, Prec.FACTOR),     #T.SLASH
            ParseRule(None, None, Prec.NONE),              #T.SEMI
            ParseRule(None, None, Prec.NONE),              #T.COLON
            ParseRule(None, None, Prec.NONE),              #T.DOT
            ParseRule(None, None, Prec.NONE),              #T.COMMA
            ParseRule(self.literal, None, Prec.NONE),              #T.STRING
            ParseRule(None, None, Prec.NONE),              #T.IDENT
            ParseRule(self.literal, None, Prec.NONE),      #T.NUMBER
            ParseRule(None, self.binary, Prec.COMPARISON),              #T.EQUALS
            ParseRule(self.group, None, Prec.NONE),     #T.LPAREN
            ParseRule(None, None, Prec.NONE),              #T.RPAREN
            ParseRule(self.array, self.index, Prec.NONE),              #T.LBRACK
            ParseRule(None, None, Prec.NONE),              #T.RBRACK
            ParseRule(None, None, Prec.NONE),              #T.LCURLY
            ParseRule(None, None, Prec.NONE),              #T.RCURLY
            ParseRule(None, self.binary, Prec.COMPARISON),              #T.LESS
            ParseRule(None, self.binary, Prec.COMPARISON),              #T.MORE
            ParseRule(None, self.binary, Prec.COMPARISON),              #T.LESSEQ
            ParseRule(None, self.binary, Prec.COMPARISON),              #T.MOREEQ
            ParseRule(None, None, Prec.NONE),              #T.PERCENT
            ParseRule(None, None, Prec.NONE),              #T.AS
            ParseRule(None, None, Prec.NONE),              #T.IS
            ParseRule(None, None, Prec.NONE),              #T.ELSE
            ParseRule(None, None, Prec.NONE),              #T.IF
            ParseRule(None, None, Prec.NONE),              #T.WHILE
            ParseRule(None, None, Prec.NONE),              #T.WITH
            ParseRule(None, None, Prec.NONE),              #T.DEFINE
            ParseRule(None, None, Prec.NONE),              #T.RETURN
            ParseRule(None, None, Prec.NONE),              #T.IMPORT
            ParseRule(self.literal, None, Prec.NONE),              #T.TRUE
            ParseRule(self.literal, None, Prec.NONE),              #T.FALSE
            ParseRule(None, None, Prec.NONE),              #T.ERROR
            ParseRule(self.unary, None, Prec.TERM),              #T.NOT
            ParseRule(None, None, Prec.NONE),              #T.MACRO
            ParseRule(self.literal, None, Prec.NONE),              #T.NULL
            ParseRule(None, None, Prec.NONE),              #T.END
        ]

    def execute(self):
        self.advance()

        while self.__current.tokenType != T.END:
            self.declaration()

        self.emitCode(OP.NULL)
        self.emitCode(OP.RETURN)

    def declaration(self):
        self.statement()

    def statement(self):
        if self.match(T.RETURN):
            self.returnStatement()
        elif self.match(T.PRINT):
            self.printStatement()
        else:
            self.expressionStatement()
        
    def expressionStatement(self):
        self.expression()
        self.consume(T.SEMI, "Expected ';'")
        self.emitCode(OP.POP)

    def returnStatement(self):
        self.advance()
        self.epression()
        self.consume(T.SEMI, "Expected ';'")
        self.emitCode(OP.RETURN)
    
    def printStatement(self):
        self.advance()
        self.expression()
        self.consume(T.SEMI, "Expected ';'")
        self.emitCode(OP.PRINT)

    def expression(self):
        self.parsePrec(Prec.ASSIGNMENT)

    #Expression Helpers
    def literal(self):
        if self.__previous.tokenType == T.NUMBER:
            self.emit(float(self.__previous.value))
        elif self.__previous.tokenType == T.STRING:
            self.emit(repr(self.__previous.value))
        elif self.__previous.tokenType == T.FALSE:
            self.emitCode(OP.FALSE)
        elif self.__previous.tokenType == T.TRUE:
            self.emitCode(OP.TRUE)
        elif self.__previous.tokenType == T.NULL:
            self.emitCode(OP.NULL)

    def array(self):
        self.emitCode(OP.MKARRAY)
        while not self.match(T.RBRACK):
            self.expression()
            self.emitCode(OP.PUSHARR)
            if not self.match(T.COMMA):
                self.consume(T.RBRACK, "expected ']'")
                return
            self.advance()
        self.advance()

    def index(self):
        self.expression()
        consume(T.RBRACK, "Expected ']'")
        self.emitCode(OP.GETARR)

    def group(self):
        self.expression()
        self.consume(T.RPAREN, "Expected ')")

    def unary(self):
        operator = self.__previous.tokenType
        self.parsePrec(Prec.UNARY)

        if operator == T.DASH:
            self.emitCode(OP.NEGATE)
        if operator == T.NOT:
            self.emitCode(OP.NOT)

    def binary(self):
        operator = self.__previous.tokenType
        rule = self.getRule(operator)
        self.parsePrec(rule.prec + 1)

        if operator == T.PLUS: self.emitCode(OP.ADD)
        elif operator == T.DASH: self.emitCode(OP.SUB)
        elif operator == T.STAR: self.emitCode(OP.MULT)
        elif operator == T.SLASH: self.emitCode(OP.DIV)
        elif operator == T.CHEVRON: self.emitCode(OP.POW)
        elif operator == T.EQUALS: self.emitCode(OP.EQUALS)
        elif operator == T.MORE: self.emitCode(OP.MORE)
        elif operator == T.MOREEQ: self.emitCode(OP.MOREEQ)
        elif operator == T.LESS: self.emitCode(OP.LESS)
        elif operator == T.LESSEQ: self.emitCode(OP.LESS)

    def parsePrec(self, prec):
        self.advance()           

        prefixRule = self.getRule(self.__previous.tokenType).prefix
        if (prefixRule is None):                                 
            self.compileError("Expect expression")                           
            return                                                 

        prefixRule()

        while prec <= self.getRule(self.__current.tokenType).prec:
            self.advance()                                               
            infixRule = self.getRule(self.__previous.tokenType).infix
            if infixRule == None: return
            infixRule()

    def getRule(self, operator):
        return self.__parseRules[operator.value]

    def advance(self):                          
        self.__previous = self.__current
                     
        self.__current = self.__tokens[self.__tokenAt] 

        self.__tokenAt += 1

        if self.__current.tokenType != T.ERROR: return

        self.error(self.__current)

    def match(self, token_type):
        return self.__current.tokenType == token_type

    def consume(self, token_type, message):
        if self.__current.tokenType == token_type:
            self.advance()
            return self.__current

        compileError(message)

    def emitCode(self, stuff):
        self.__codes.append(stuff.value)

    def emit(self, stuff):
        self.__codes.append(stuff)

    def scanError(self, token):
        raise ScanError(token.value)

    def compileError(self, message):
        raise CompileError(message + " at token " + str(self.__tokenAt))

    def printOut(self):
        for item in self.__codes:
            print("OPCODE:", OP(item).name if type(item) == int else item)

    def setTokens(self, tokens):
        self.__tokens = tokens

    @property
    def chunk(self):
        return self.__codes
  

        