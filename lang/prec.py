from enum import Enum

class Prec(Enum):

    NONE = 0                  
    ASSIGNMENT = 1          
    OR = 2      
    AND = 3              
    EQUALITY = 4     
    COMPARISON = 5
    TERM = 6
    FACTOR = 7
    EXPONENT = 8       
    UNARY = 9 
    CALL = 10
    PRIMARY = 11

    def __add__(self, other):
        assert type(other) == int

        return Prec(self.value + other)

    def __lt__(self, other):
        return self.value < other.value
    
    def __gt__(self, other):
        return self.value > other.value

    def __le__(self, other):
        return not self > other

    def __ge__(self, other):
        return not self < other

    def __eq__(self, other):
        return self.value == other.value