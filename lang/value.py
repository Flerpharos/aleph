import math
from lang.error import RuntimeError

class Value:

    def __init__(self, value):
        #print("VAL",type(value))
        self.value = value

    def neg(self):
        if isinstance(self.value, float):
            return Value(-self.value)
        else:
            raise RuntimeError("Invalid operand type for numeric negation")

    def negate(self):
        if isinstance(self.value, str):
            return Value(self.value[::-1])
        elif isinstance(self.value, bool):
            return Value(not self.value)
        elif self.value == None:
            return Value(None)
        elif isinstance(self.value, list):
            return Value(self.value.reverse())
        else:
            raise RuntimeError("Invalid operand type for negation.")

    def add(self, other):
        if isinstance(self.value, list) and not type(self.value) == type(other.value):
            self.value.append(other)
            return self

        if type(self.value) != type(other.value):
            raise RuntimeError("Operand types must match for addition.")

        if isinstance(self.value, str):
            return Value(self.value + other.value)
        elif isinstance(self.value, float):
            return Value(self.value + other.value)
        elif isinstance(self.value, list):
            return Value(self.value + other.value)
        elif isinstance(self.value, dict):
            raise Value(self.value.update(other.value))
        else:
            raise RuntimeError("Invalid operand type for addition.")

    def sub(self, other):
        if type(self.value) != type(other.value):
            raise RuntimeError("Operand types must match for subtraction.")

        if isinstance(self.value, float):
            return Value(self.value - other.value)
        raise RuntimeError("Invalid operand type for subtraction.")

    def mul(self, other):
        if isinstance(self.value, str) and isinstance(other.value, float) and self.isInt(other.value):
            return Value(self.value * int(other.value))
        elif isinstance(other.value, str) and isinstance(self.value, float) and self.isInt(self.value):
            return Value(other.value * int(self.value))
        elif isinstance(self.value, float) and isinstance(other.value, float):
            return Value(self.value * other.value)
        else: raise RuntimeError("Invalid operands for multiplication.")

    def div(self, other):
        if type(self.value) == float and type(other.value) == float:
            return Value(self.value / other.value)
        raise RuntimeError("Invalid operands for division.")

    def pow(self, other):
        if type(self.value) == float and type(other.value) == float:
            return Value(math.pow(self.value, other.value))
        raise RuntimeError("Invalid operands for exponentiation.")

    def eq(self, other):
        if type(self.value) != type(other.value):
            return Value(False)

        if type(self.value) == None:
            return Value(True)
        elif type(self.value) == list:
            if len(self.value) != len(other.value): return Value(False)
            for i in range(len(self.value)):
                if self.value[i] != other.value[i]: return Value(False)
            return Value(True)
        elif type(self.value) == dict:
            for key in self.value:
                try:
                    if other.value[key] != self.value[key]: return Value(False)
                except KeyError:
                    return Value(False)
            return Value(True)
        return self.value == other.value

    def ne(self, other):
        return Value(not self.eq(other).value)

    def lt(self, other):
        return Value(self.value < other.value)
    
    def gt(self, other):
        return Value(self.value > other.value)

    def le(self, other):
        return Value(not self.gt(other))

    def ge(self, other):
        return Value(not self.lt(other))

    def __str__(self):
        if type(self.value) == list:
            data = "["
            for item in self.value:
                data += str(item) + ", "
            data = data[0:len(data)-2] + "]"
            return data
        if type(self.value) == dict:
            data = "{"
            for key in self.value:
                data += str(key) + ":" + str(self.value[key]) + ","
            data = data[0:len(data)-2] + "}"
            return data
        if type(self.value) == float and self.isInt(self.value):
                return str(int(self.value))
        else:
            return str(self.value)

    def isInt(self, value):
        return math.isclose(value % 1, 0) or math.isclose(value % 1, 1)


            